import React from 'react';
import Avanzadagrid from './components/Avanzadagrid';

function App() {
  return (
    <Avanzadagrid />
  );
}

export default App;
