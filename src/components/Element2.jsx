import React from 'react'
import './styles/Element2.css'

const Element2 =()=>(
    <article>
        <p className="alerta letragrande">parrafo 1</p>
        <p className="alerta">parrafo 2</p>
        <p>parrafo 3</p>
        <p className="letragrande">parrafo 4</p>
        <p>parrafo 5</p>
        <h2 className="alerta">h2</h2>
        <div></div>
        <div id="amarillo"></div>
    </article>
)

export default Element2