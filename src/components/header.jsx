import React from 'react'
import './styles/header.css'
import PropTypes from 'prop-types'

const Header = ({title}) => (
    <div>
      <h1 className="Header">{title}</h1>
    </div>
)
Header.propTypes={
    title:PropTypes.string
}
Header.defaultProps={
    title:"no hay titulo"
}

export default Header