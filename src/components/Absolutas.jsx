import React from 'react'
import './styles/Absolutas.css'

const Absolutas = () => (
    <article>
        <p>Parrafo Normal</p>
        <p class="in">Parrafo IN</p>
        <p class="cm">Parrafo CM</p>
        <p class="mm">Parrafo MM</p>
        <p class="pt">Parrafo PT</p>
        <p class="pc">Parrafo PC</p>
    </article>
)

export default Absolutas