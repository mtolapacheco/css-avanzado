import React from 'react'
import './styles/Atributo.css'

const Atributo = ()=>(
    <article>
        <p className="azul">Ejemplo</p>
        <p className="verde letra_grande">Ejemplo</p>
        <p>Ejemplo</p>
        <p id="rojo">Ejemplo</p>
        <h2 title="primer ejemplo de h2">Primer h2</h2>
        <h2 title="segundo ejemplo de h2">Segundo h2</h2>
        <a href="#" lang="es-ES">Español de España</a>
        <a href="#" lang="es-CL">Español de Colombia</a>
        <a href="#" lang="es-MX">Español de Mexico</a>
    </article>
)

export default Atributo