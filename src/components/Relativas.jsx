import React from 'react'
import './styles/Relativas.css'

const Relativas = () => (
    <article>
        <p>Parrafo Normal</p>
        <p class="em">Parrafo EM</p>
        <p class="ex">Parrafo EX</p>
        <p class="px">Parrafo PX</p>
        <div>
            <p>Ejemplo em heredado</p>
            <p class="rem">Ejemplo rem</p>
        </div>
    </article>
)

export default Relativas