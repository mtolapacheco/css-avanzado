import React from 'react'
import './styles/Element.css'

const Element= () => (
    <article>
        <header>
            <div>
                <h2>
                    titulo secundario dentro del header y del div
                </h2>
            </div>
            <h2>
                titulo secundario DENTRO del header y no del div
            </h2>
        </header>
        <h2>
            titulo secundario FUERA del header
        </h2>
        <h3>titulo h3</h3>
    </article>
)
export default Element