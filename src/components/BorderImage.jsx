import React from 'react'
import './styles/BorderImage.css'

const BorderImage = () => (
    <article>
        <div>
            border-image: url(border.png) 50 round;
        </div>
        <div id="borderimg2">border-image: url(border.png) 20% round;</div>
        <div id="borderimg3">border-image: url(border.png) 30% round;</div>
    </article>
)

export default BorderImage