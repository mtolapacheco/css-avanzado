import React from 'react'
import './styles/Porcentuales.css'

const Porcentuales = () => (
    <article>
        <div class="decimal"></div>
        <div class="decimal_alpha"></div>
        <div class="porcentaje"></div>
        <div class="hexadecimal"></div>
        <div class="hsl"></div>
        <div class="hsla"></div>
    </article>
)

export default Porcentuales